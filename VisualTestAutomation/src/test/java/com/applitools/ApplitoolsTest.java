package com.applitools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.applitools.eyes.*;
import com.applitools.eyes.selenium.ClassicRunner;
import com.applitools.eyes.selenium.Eyes;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ApplitoolsTest {



	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();


		WebDriver driver = new ChromeDriver();

		// Initialize the Runner for your test.
		EyesRunner runner = new ClassicRunner();

		// Initialize the eyes SDK
		Eyes eyes = new Eyes(runner);

		// Change the APPLITOOLS_API_KEY API key with yours
		eyes.setApiKey("API key"); //- otherwise it will fail


		// Set AUT's name, test name and viewport size (width X height)
		// We have set it to 800 x 600 to accommodate various screens. Feel free to
		// change it.
		eyes.open(driver, "Demo App", "Smoke Test", new RectangleSize(800, 600));

		// Navigate the browser to the "ACME" demo app.
		driver.get("https://the-internet.herokuapp.com/dynamic_content");

		eyes.setMatchLevel(MatchLevel.LAYOUT);

		// This line takes the screenshot
		eyes.checkWindow("Content window");


		// End the test.
		eyes.closeAsync();

		// Close the browser.
		driver.quit();

		// If the test was aborted before eyes.close was called, ends the test as
		// aborted.
		eyes.abortIfNotClosed();


		// Wait and collect all test results
		TestResultsSummary allTestResults = runner.getAllTestResults();

		// Print results
		System.out.println(allTestResults);




	}

}
