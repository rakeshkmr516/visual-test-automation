package com.pdf;

import java.io.IOException;

import de.redsix.pdfcompare.CompareResult;
import de.redsix.pdfcompare.PdfComparator;

public class PDFCompareSample {

	public static void main(String[] args) throws IOException {
	
		final CompareResult result = new PdfComparator("./src/test/resources/pdf invoice1.pdf", "./src/test/resources/pdf invoice2.pdf").compare();
		if (result.isNotEqual()) {
		    System.out.println("Differences found!");
		}
		if (result.isEqual()) {
		    System.out.println("No Differences found!");
		}
		if (result.hasDifferenceInExclusion()) {
		    System.out.println("Differences in excluded areas found!");
		}
		
	
		
		result.writeTo("./PDFCompareResult/pdfresult");
	
	}

}
